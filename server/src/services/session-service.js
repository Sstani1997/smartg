import bcrypt from 'bcrypt';
import usersData from '../data/users-data.js';
import createToken from '../auth/create-token.js';
import { sendEmailTemplate } from '../common/send-mails.js';
import helpers from '../common/helpers.js';

const createNewPassword = async (requestId) => {
  const ONE_HOUR = 60 * 60 * 1000;
  const { error, request } = await usersData.getRequest(requestId);
  if(error){
    return { errorCode: 403, response: 'Error getting password request' };
  } else if (!request) {
    return { errorCode: 403, response: 'Your password was already reset' };
  } else if (!helpers.isExpired(request.TIME, ONE_HOUR)) {
    return { errorCode: 403, response: 'Password request expired' };
  } else {
    const newPassword = 'user' + Math.round(Math.random() * 1000);
    const user = {
      id: request.UserID,
      password: await bcrypt.hash(newPassword, 10),
    };
    const { error, response } = await usersData.updatePassword(user);
    if (error) {
      return { errorCode: 400, response };
    } else {
      const { response } = await usersData.getUserBy('id', request.UserID);
      const emailInfo = {
        email: response.email,
        password: newPassword,
      };
      sendEmailTemplate(emailInfo, 'forgotten');
    }
    return { errorCode: null, response };
  }
};



const createPasswordReq = async (email) => {
  const { error, response } = await usersData.getUserBy('email', email);
  if (error) {
    return { error: 400, response };
  } else {
    const result = await usersData.createNewPasswordRequest(response.id);
    console.log(result);
    const emailInfo = {
      email: email,
      link: `http://localhost:3000/reset/success?ID=${result.requestId}`,
    };
    sendEmailTemplate(emailInfo, 'forgottenPasswordRequest');
  }
  return { error: null, response };

};

const deletePasswordReq = async (requestId) => {
  return await usersData.deleteRequest(requestId);
};


const validateUser = async (user) => {
  const { error, response } = await usersData.getUserBy('email', user.email);
  if (error) {
    return { error, response };
  }
  if (await bcrypt.compare(user.password, response.password)) {
    const { id, fullname, role } = response;
    const token = createToken({ id, fullname, role });

    return {
      error: null,
      response: { token },
    };
  }

  return {
    error: 400,
    response: 'Invalid password!',
  };
};

/**
 * 
 * @param {*} token 
 * @returns 
 */
const logOutUser = async (token) => {
  await usersData.logoutUser(token);
  return { error: null, response: 'You are successfully logged out!' };
};


export default {
  validateUser,
  createPasswordReq,
  deletePasswordReq,
  createNewPassword,
  logOutUser,
};