import helpers from '../common/helpers.js';
import ordersData from '../data/orders-data.js';


const getOrders = async (query) => {
    const result = await ordersData.getOrders(query);
    return helpers.returnResult(result);

};

export default {
    getOrders,

};