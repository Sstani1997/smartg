import helpers from '../common/helpers.js';
import servicesData from '../data/services-data.js';


const getAllService = async (query, id) => {
    const result = await servicesData.getAllServices(query, id);
  
    return helpers.returnResult(result);
  };

const getService = async (serviceId) => {
    const result = await servicesData.getServiceById(serviceId);
  
    return helpers.returnResult(result);
  };

  const deleteService = async (serviceId) => {
    const result = await servicesData.deleteService(serviceId);
  
    return helpers.returnResult(result);
  };
  
  const updateService = async (serviceId, service) => {
    const result = await servicesData.updateService(serviceId, service);
  
    return helpers.returnResult(result);
  };

  export default {
    getService,
    getAllService,
    deleteService,
    updateService,
  };
  