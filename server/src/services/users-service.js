import bcrypt from 'bcrypt';
import usersData from '../data/users-data.js';
import { sendEmailTemplate } from '../common/send-mails.js';
import helpers from '../common/helpers.js';

const getAllUsers = async (query) => {
  const users = await usersData.getAll(query);
  return helpers.returnResult(users);
};

const getUsersList = async () => {
  const users = await usersData.getList();
  return helpers.returnResult(users);
};

const getUserBy = async (column, value) => {
  const result = await usersData.getUserBy(column, value);
  return helpers.returnResult(result);
};


const updateUser = async (id, user) => {
  const { error, response } = await usersData.updateUser(id, user);
  if (error) {
    return { error, response };
  }

  return { error: null, response };
};


const createUser = async (user) => {
  const realPassword = 'user' + Math.round(Math.random() * 1000);
  user.password = await bcrypt.hash(realPassword, 10);

  // user.password = 'user' + Math.round(Math.random()*1000);
  const { error, response } = await usersData.createUser(user);
  if (error) {
    return { error: 400, response };
  } else {
    const emailInfo = {
      name: user.firstName + ' ' + user.lastName,
      email: user.email,
      password: realPassword,
    };
    sendEmailTemplate(emailInfo, 'newUser');
  }
  return { error: null, response };

};

export default {
  getAllUsers,
  getUserBy,
  createUser,
  updateUser,
  getUsersList,
};
