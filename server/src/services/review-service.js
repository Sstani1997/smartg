import reviewsData from '../data/reviews-data.js';
import helpers from '../common/helpers.js';

const createReview = async (userId, bookId, body) => {
  const result = await reviewsData.create(userId, bookId, body);

  return helpers.returnResult(result);
};

const getReview = async (id) => {
  const result = await reviewsData.getById(id);

  return helpers.returnResult(result);
};

const getAllReviews = async (query, id) => {
  const result = await reviewsData.getAll(query, id);

  return helpers.returnResult(result);
};

const deleteReview = async (userId, reviewId) => {
  const result = await reviewsData.deleteReview(userId, reviewId);

  return helpers.returnResult(result);
};

const updateReview = async (userId, reviewId, review) => {
  const result = await reviewsData.updateReview(userId, reviewId, review);

  return helpers.returnResult(result);
};

const likesReview = async (userId, reviewId, body) => {
  if (![1, -1].includes(body.vote)) {
    return {
      error: 422,
      response: 'Vote has to be 1 or -1',
    };
  }
  const { error, response } = await reviewsData.likeReview(userId, reviewId, body.vote);
  if (error) {
    return {
      error: 404,
      response: 'No such review',
    };
  }

  return { response };
};

export default {
  createReview,
  getAllReviews,
  getReview,
  deleteReview,
  updateReview,
  likesReview,
};
