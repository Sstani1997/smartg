import helpers from '../common/helpers.js';
import carsData from '../data/cars-data.js';

const getCarsByParams = async (query) => {
  const result = await carsData.getCarsByParams(query);
  return helpers.returnResult(result);
};

const getCarById = async (carId) => {
  const result = await carsData.getCarById(carId);
  return helpers.returnResult(result);
};

const createCar = async (body) => {
  const result = await carsData.createCar(body);
  return helpers.returnResult(result);
};
const updateCarById = async (carId, body) => {
  const result = await carsData.updateCarById(carId, body);
  return helpers.returnResult(result);
};

const getBrandsModels = async (query) => {
  const result = await carsData.getBrandsModels(query);
  return helpers.returnResult(result);
};

export default {
  getCarsByParams,
  getCarById,
  getBrandsModels,
  createCar,
  updateCarById,
};