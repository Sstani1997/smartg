import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import usersController from './controllers/users-controller.js';
import authMiddleware from './auth/auth.middleware.js';
// import roleAuth from './middlewares/common/role-auth.js';
import userGuard from './middlewares/common/user-guard.js';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import dotenv from 'dotenv';
import carsController from './controllers/cars-controller.js';
import ordersController from './controllers/orders-controller.js';
import sessionController from './controllers/session-controller.js';
import servicesController from './controllers/services-controller.js';
const config = dotenv.config().parsed;

const PORT = config.PORT;
const app = express();
app.use(cors());
app.use(helmet());
app.use(express.json());

passport.use(jwtStrategy);
app.use(passport.initialize());

app.use('/api/session', sessionController);
app.use('/api/services', servicesController);
app.use('/api/orders', authMiddleware, userGuard, ordersController);
app.use('/api/users', authMiddleware, userGuard, usersController);
app.use('/api/cars', authMiddleware, userGuard, carsController);
app.use('/api/employee', usersController); //userGuard, roleAuth('employee'),

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
