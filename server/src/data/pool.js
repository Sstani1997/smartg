import mariadb from 'mariadb';
import dotenv from 'dotenv';

const config = dotenv.config().parsed;

const pool = mariadb.createPool({
  host: config.HOST,
  user: config.USER,
  password: config.PASSWORD,
  database: config.DATABASE,
  port: config.DBPORT,
  connectionLimit: 1,
});

export default pool;