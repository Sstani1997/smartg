import pool from './pool.js';
import helpers from '../common/helpers.js';


/**
 *
 * @param {object} req Query request
 * @returns {promise} SQL result set
 */
const getAll = async (query) => {
  const tableFields = ['fullName', 'email', 'address', 'plate', 'brand', 'model', 'vin', 'phone', 'year'];
  const sorting = helpers.sorting(query, tableFields);
  const pagination = helpers.pagination(query);
  const search = helpers.searchQueryParts(tableFields, query);
  query.from = query.from || 0;
  query.to = query.to || 3000;
  // const yearRange = (query.from || query.to) ? `and year between '${query.from}' and '${query.to}'` : '';

  const sql = `
  SELECT
      u.id,
      concat_ws(' ', u.firstName, u.lastName) as fullName,
      u.firstName,
      u.lastName,
      c.plate,
      c.vin,
      c.year,
      c.id as carId,
      m.name as model,
      b.name as brand,
      u.email,
      u.phone,
      u.address
  FROM users as u
  LEFT JOIN cars as c on
    u.id = c.customer_id
  LEFT JOIN models as m on
    m.id = c.model_id
  LEFT JOIN brands as b on
    b.id = m.brand_id
    ${search}
    ${sorting}
    ${pagination}
    `;
  const result = await pool.query(sql);
  return { response: result };
};

const getList = async () => {
  const sql = `
  SELECT
      u.id,
      concat_ws(' ', u.firstName, u.lastName) as fullName,
      u.email,
      u.phone,
      u.address
  FROM users as u
    `;
  const result = await pool.query(sql);
  return { response: result };
};

const createUser = async (user) => {
  const existingPhone = (await pool.query('SELECT * FROM users u WHERE u.phone = ?', [user.phone]))[0];
  const existingEmail = (await pool.query('SELECT * FROM users u WHERE u.email = ?', [user.email]))[0];
  if (existingPhone) {
    return { error: 400, response: { error: 'Phone is already registered!' } };
  }

  if (existingEmail) {
    return { error: 400, response: { error: 'Email is already registered!' } };
  }
  const sql = `
    INSERT INTO users ( firstName, lastName, email, phone, address, password)
    VALUES (?, ?, ?, ?, ?, ?)
  `;
  const result = await pool.query(sql,
    [
      user.firstName,
      user.lastName,
      user.email,
      user.phone,
      user.address,
      user.password,
    ]);

  const sql2 = `
    SELECT
      *
    FROM users u
    WHERE u.id = ?
  `;

  const createdUser = (await pool.query(sql2, [result.insertId]))[0];

  return { error: null, response: createdUser };
};

const updateUser = async (id, user) => {
  const existingEmail = (await pool.query('SELECT * FROM users u WHERE u.email = ? AND id != ?', [user.email, id]))[0];
  const existingPhone = (await pool.query('SELECT * FROM users u WHERE u.phone = ? AND id != ?', [user.phone, id]))[0];

  if (existingEmail) {
    return {
      error: 400,
      response: { error: 'Email is already registered!' },
    };
  }
  if (existingPhone) {
    return {
      error: 400,
      response: { error: 'Phone is already registered!' },
    };
  }
  const sql = `
    UPDATE users SET
      firstName = ?,
      lastName = ?,
      email = ?,
      phone = ?,
      address = ?
    WHERE id = ?
  `;
  await pool.query(sql,
    [
      user.firstName,
      user.lastName,
      user.email,
      user.phone,
      user.address,
      id,
    ]);

  const result = await getUserBy('id', id);

  return { error: null, response: result };
};


const updatePassword = async (user) => {
  const existingEmail = (await pool.query('SELECT * FROM users u WHERE u.id = ?', [user.id]))[0];

  if (!existingEmail) {
    return {
      error: 400,
      response: { error: 'No such user!' },
    };
  }
  const sql = `
    UPDATE users SET password = ?
    WHERE id = ?
  `;
  await pool.query(sql, [user.password, user.id]);

  return { error: null, response: 'Your password has been updated' };
};


const createNewPasswordRequest = async (userId) => {

  const sql = `
  INSERT INTO password_change_requests (UserID) VALUES (?) ;
  `;
  const result = await pool.query(sql, [userId]);
  return { error: null, requestId: result.insertId };
};

const getRequest = async (requestId) => {
  const sql = `
  SELECT TIME, UserID FROM password_change_requests WHERE id = ?
  `;
  const result = await pool.query(sql, [requestId]);
  return { error: null, request: result[0] };
};

const deleteRequest = async (requestId) => {
  const sql = `
  DELETE FROM password_change_requests WHERE id = ?
  `;
  await pool.query(sql, [requestId]);
  return { error: null, request: 'Password request deleted successfully' };
};



const getUserBy = async (column, value) => {
  const sql = `
  SELECT
    u.id,
    u.email,
    u.phone,
    u.firstName,
    u.lastName,
    concat_ws(' ', u.firstName, u.lastName) as fullname,
    u.password as password,
    u.role as role
  FROM users as u WHERE ${column} = ?
  `;
  const existingUser = (await pool.query(sql, [value]))[0];
  if (!existingUser) {
    return {
      error: 404,
      response: 'No such user!',
    };
  }
  return {
    error: false,
    response: existingUser,
  };
};

const logoutUser = async (token) => {
  return await pool.query('INSERT INTO tokens (token) VALUES (?)', [token]);
};

export default {
  getAll,
  getList,
  createUser,
  updateUser,
  getUserBy,
  logoutUser,
  updatePassword,
  deleteRequest,
  createNewPasswordRequest,
  getRequest,
};