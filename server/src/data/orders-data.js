import pool from './pool.js';
import helpers from '../common/helpers.js';

const getOrders = async (query) => {
  const tableFields = ['fullName', 'email', 'address', 'plate', 'brand', 'model', 'vin', 'phone', 'year'];
  const sorting = helpers.sorting(query, tableFields);
  const pagination = helpers.pagination(query);
  const search = helpers.searchQueryParts(tableFields, query);
  // query.from = query.from || 0;
  // query.to = query.to || 3000;
  // const yearRange = (query.from || query.to) ? `and year between '${query.from}' and '${query.to}'` : '';

  const sql = `
  SELECT
      u.id,
      concat_ws(' ', u.firstName, u.lastName) as fullName,
      u.firstName,
      u.lastName,
      c.plate,
      c.vin,
      c.year,
      c.id as carId,
      m.name as model,
      b.name as brand,
      u.email,
      u.phone,
      u.address
  FROM orders as o
  LEFT JOIN cars as c on
    c.id = o.car_id
  LEFT JOIN users as u on
    u.id = c.customer_id
  LEFT JOIN models as m on
    m.id = c.model_id
  LEFT JOIN brands as b on
    b.id = m.brand_id
    ${search}
    ${sorting}
    ${pagination}
    `;
  const result = await pool.query(sql);
  return { response: result };
};

export default {
  getOrders,
};