import pool from './pool.js';
import helpers from '../common/helpers.js';
import SqlString from 'sqlstring';


const getCarsByParams = async (query) => {
  const tableFields = ['brand', 'model', 'year', 'transmission', 'plate', 'vin', 'fullName', 'phone'];
  const sorting = helpers.sorting(query, tableFields);
  const pagination = helpers.pagination(query);
  const search = helpers.searchQueryParts(tableFields, query);

  const sql = `
    SELECT
      c.id,
      c.customer_id as customerId,
      b.id as brandId,
      b.name as brand,
      m.id as modelId,
      m.name as model,
      c.year,
      c.engine,
      c.transmission,
      c.vin,
      c.plate,
      b.logoURL,
      concat(u.firstName, ' ', u.lastName) as fullName,
      u.phone
    FROM
      cars as c
    JOIN models as m on
      c.model_id = m.id
    JOIN brands as b on
      b.id = m.brand_id
    JOIN users as u on
      u.id = c.customer_id
    ${search}
    ${sorting}
    ${pagination}
    `;
  const response = await pool.query(sql);
  return { error: null, response };
};


const getCarById = async (carId) => {
  const sql = `
    SELECT
      c.id,
      u.id as customerId,
      b.id as brandId,
      b.name as brand,
      m.id as modelId,
      m.name as model,
      c.year,
      c.transmission,
      c.vin,
      c.plate,
      c.engine,
      b.logoURL,
      concat(u.firstName, ' ', u.lastName) as fullName,
      u.phone
    FROM
      cars as c
    JOIN models as m on
      c.model_id = m.id
    JOIN brands as b on
      b.id = m.brand_id
    JOIN users as u on
      u.id = c.customer_id
  WHERE c.id = ${SqlString.escape(carId)}
    `;
  const result = (await pool.query(sql))[0];
  if (!result) {
    return {
      error: 404,
      response: 'No such car!',
    };
  }
  return { error: null, response: result };
};

const getBrandsModels = async () => {
  const sql = `
    SELECT
      m.id,
      m.id as value,
      concat(b.name,' ', m.name) as label,
      b.id as brandId,
      b.name as brand,
      m.id as modelId,
      m.name as model,
      b.logoURL
    FROM
      models as m
    JOIN brands as b on
      b.id = m.brand_id
    WHERE m.is_deleted = 0
    ORDER BY b.name, m.name
    `;

  const response = await pool.query(sql);
  return { error: null, response };
};

const updateCarById = async (carId, body) => {
  const sql = `
    UPDATE cars set
      model_id = ${SqlString.escape(body.modelId)},
      vin = ${SqlString.escape(body.vin)},
      plate = ${SqlString.escape(body.plate)},
      engine = ${SqlString.escape(body.engine)},
      transmission = ${SqlString.escape(body.transmission)},
      year = ${SqlString.escape(body.year)},
      customer_id = ${SqlString.escape(body.customerId)}
    WHERE id = ${SqlString.escape(carId)}
    `;

  await pool.query(sql);
  const result = await getCarById(carId);
  return { error: null, response: result };
};

const createCar = async (body) => {
  const sql = `
    INSERT INTO cars set
      model_id = ${SqlString.escape(body.modelId)},
      vin = ${SqlString.escape(body.vin)},
      plate = ${SqlString.escape(body.plate)},
      engine = ${SqlString.escape(body.engine)},
      transmission = ${SqlString.escape(body.transmission)},
      year = ${SqlString.escape(body.year)},
      customer_id = ${SqlString.escape(body.customerId)}
    `;

  const car = await pool.query(sql);
  const result = await getCarById(car.insertId);
  return { error: null, response: result };
};



export default {
  getCarsByParams,
  getCarById,
  getBrandsModels,
  updateCarById,
  createCar,
};