import pool from './pool.js';
import helpers from '../common/helpers.js';


const getAllServices = async (query) => {
  const tableFields = ['parentName', 'price', 'name'];
  const searchParams = tableFields.map(() => `%${query.search}%`);
  const sorting = helpers.sorting(query, tableFields);
  const pagination = helpers.pagination(query);
  const search = helpers.searchQueryParts(tableFields,query);
  query.from = query.from || 0;
  query.to = query.to || 1000;
  const priceRange = (query.from || query.to) ? `and s2.price between '${query.from}' and '${query.to}'` : '';


  const sql = `
    SELECT 
      s2.id,
      concat_ws(' ', s1.name, s2.name) as fullName,
      s1.name as parentName, 
      s2.name as name,
      s2.price 
    FROM
      services as s1
    INNER JOIN services as s2 on 
      s2.parent_id = s1.id
    WHERE
      s2.is_deleted = 0
      ${search}
      ${priceRange}
      ${sorting}
      ${pagination}
      `;
  return { response: await pool.query(sql, [...searchParams]) };
};
const deleteService = async (serviceId) => {
  const { response } = await getServiceById(serviceId);
  const sql = `
    UPDATE services
      SET is_deleted = 1
    WHERE  id = ?`;
  const isDeleted = await pool.query(sql, [serviceId]);
  return isDeleted.affectedRows ?
    { response } :
    { error: 405, response: 'Operation not allowed' };
};

const updateService = async (serviceId, service) => {
  const sqlCheck = `
    SELECT * from services as s
    WHERE
      s.is_deleted = 0 AND
      s.id = ?
    `;
  const checkExist = await pool.query(sqlCheck, [serviceId]);
  if (checkExist[0]) {
    await pool.query('UPDATE services as s SET s.name = ?, s.price = ? WHERE s.id = ?', [
      service.name,
      service.price,
      serviceId,
    ]);

    return await getServiceById(serviceId);
  }
  return { error: 405, response: 'Operation not allowed!' };
};


const getServiceById = async (serviceId) => {

  const sql = `
    SELECT
      *
    FROM
      services as s
    WHERE s.is_deleted = 0 AND s.id = ?
    `;

  const response = (await pool.query(sql, serviceId))[0];
  if (response) {
    return { response };
  }
  return { error: 404, response: 'Service is not found!' };
};

export default {
  getAllServices,
  deleteService,
  updateService,
  getServiceById,
};