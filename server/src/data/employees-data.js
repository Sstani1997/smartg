import pool from './pool.js';
import helpers from '../common/helpers.js';
// import variables from '../common/variables.js';


const getAllUsers = async (query) => {
    const tableFields = ['firstName', 'lastName', 'email', 'phone'];
    const searchParams = tableFields.map(() => `%${query.search}%`);
    const sorting = helpers.sorting(query, tableFields);
    const pagination = helpers.pagination(query);
    const searchFields = tableFields.map(e => `${e} LIKE ?`).join(' OR ');
    const search = query.search ? `HAVING ${searchFields}` : '';
  
    const sql = `
    SELECT
      u.firstName,
      u.lastName,
      u.email,
      u.phone
    FROM
      users as u
    ${search}
    ${sorting}
    ${pagination}
    `;
    const response = await pool.query(sql, searchParams);
    return { error: null, response };
  };



  
  export default {
    getAllUsers,
  };