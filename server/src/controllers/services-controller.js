import express from 'express';
import variables from '../common/variables.js';
import validateBody from '../middlewares/common/validate-body.js';
import servicesService from '../services/services-service.js';
import serviceValidator from '../validators/service-validator.js';

const servicesController = express.Router();

servicesController

// View all services
.get('/', async (req, res) => {
    try {
      const { error, response } = await servicesService.getAllService(req.query);
      if (error) {
        return res.status(error).json({ error: response });
      }
      res.status(200).json(response);
    } catch (error) {
      res.status(400).json(error);
    }
  })


  // Get service info by id
  .get('/:serviceId', async (req, res) => {
      console.log(req.params.id);
    try {
      const { error, response } = await servicesService.getService(req.params.serviceId);
      if (error) {
        return res.status(error).json({ error: response });
      }
      res.status(200).json(response);
    } catch (error) {
      res.status(400).json(error);
    }
  })


  // Update service info by id
  .put('/:serviceId', validateBody('validateService', serviceValidator), async (req, res) => {
    try {
      const { error, response } = await servicesService.updateService(req.params.serviceId, req.body);
      if (error) {
        return res.status(error).json({ error: response });
      }
      res.status(200).json(response);
    } catch (error) {
      res.status(400).json(error);
    }
  })

  // delete service
  .delete('/:serviceId', async (req, res) => {
    try {
      const { error, response } = await servicesService.getService(req.params.serviceId);
      if (error) {
        res.status(error).json(response);
      } else {
        const { error, response } = await servicesService.deleteService(req.params.serviceId);
        if (error) {
          res.status(error).json({ error: response });
        } else {
          res.status(200).json(response);
        }
      }
    } catch (error) {
      res.status(400).json(variables.DEFAULT_ERR);
    }
  })
  ;



  export default servicesController;