import express from 'express';
import variables from '../common/variables.js';
import validateBody from '../middlewares/common/validate-body.js';
import createUserValidator from '../validators/create-user-validator.js';
// import validateUserValidator from '../validators/validate-user-validator.js';
import usersService from '../services/users-service.js';
// import variables from '../common/variables.js';

const usersController = express.Router();

usersController

  // Get all users
  .get('/', async (req, res) => {
    try {
      const { error, response } = await usersService.getAllUsers(req.query);
      if (error) {
        res.status(error).json({ error: response });
      } else {
        res.status(200).json(response);
      }
    } catch (error) {
      console.log(error);
      res.status(400).json(variables.DEFAULT_ERR);
    }
  })

  .get('/list', async (req, res) => {
    try {
      const { error, response } = await usersService.getUsersList();
      if (error) {
        res.status(error).json({ error: response });
      } else {
        res.status(200).json(response);
      }
    } catch (error) {
      console.log(error);
      res.status(400).json(variables.DEFAULT_ERR);
    }
  })

  // Get user by id
  .get('/:userId', async (req, res) => {
    try {
      const { error, response } = await usersService.getUserBy('id', req.params.userId);
      if (error) {
        res.status(error).json({ error: response });
      } else {
        res.status(200).json(response);
      }
    } catch (error) {
      console.log(error);
      res.status(400).json(variables.DEFAULT_ERR);
    }
  })


  // Update user
  .put('/:userId', validateBody('createUser', createUserValidator), async (req, res) => {
    try {
      const { error, response } = await usersService.updateUser(req.params.userId, req.body);
      if (error) {
        return res.status(error).json({ error: response });
      }
      res.status(200).json(response);
    } catch (error) {
      res.status(400).json(error);
    }
  })

  // Register user
  .post('/register', validateBody('createUser', createUserValidator), async (req, res) => {
    try {
      const { error, response } = await usersService.createUser(req.body);
      if (error) {
        return res.status(error).json({ error: response });
      }
      res.status(200).json(response);
    } catch (error) {
      res.status(400).json(error);
    }
  });


export default usersController;