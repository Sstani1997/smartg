import express from 'express';
import validateBody from '../middlewares/common/validate-body.js';
import carsService from '../services/cars-service.js';
import carValidator from '../validators/car-validator.js';

const carsController = express.Router();

carsController

  // View cars with filters
  .get('/', async (req, res) => {
    try {
      const { error, response } = await carsService.getCarsByParams(req.query);
      if (error) {
        return res.status(error).json({ error: response });
      }
      res.status(200).json(response);
    } catch (error) {
      res.status(400).json(error);
    }
  })

  // Get brands & models
  .get('/models', async (req, res) => {
    try {
      const { error, response } = await carsService.getBrandsModels(req.query);
      if (error) {
        return res.status(error).json({ error: response });
      }
      res.status(200).json(response);
    } catch (error) {
      res.status(400).json(error);
    }
  })

  // Get car info by id
  .get('/:carId', async (req, res) => {
    try {
      const { error, response } = await carsService.getCarById(req.params.carId);
      if (error) {
        return res.status(error).json({ error: response });
      }
      res.status(200).json(response);
    } catch (error) {
      res.status(400).json(error);
    }
  })

    // Create car
    .post('/', validateBody('validateCar', carValidator), async (req, res) => {
      try {
        const { error, response } = await carsService.createCar(req.body);
        if (error) {
          return res.status(error).json({ error: response });
        }
        res.status(200).json(response);
      } catch (error) {
        res.status(400).json(error);
      }
    })

  // Update car info by id
  .put('/:carId', validateBody('validateCar', carValidator), async (req, res) => {
    try {
      const { error, response } = await carsService.updateCarById(req.params.carId, req.body);
      if (error) {
        return res.status(error).json({ error: response });
      }
      res.status(200).json(response);
    } catch (error) {
      res.status(400).json(error);
    }
  })


  ;



export default carsController;
