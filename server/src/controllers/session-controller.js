import express from 'express';
import variables from '../common/variables.js';
import validateBody from '../middlewares/common/validate-body.js';
import validateUserValidator from '../validators/validate-user-validator.js';
import sessionService from '../services/session-service.js';

const sessionController = express.Router();

sessionController


// Login user
.post('/login', validateBody('validateUser', validateUserValidator), async (req, res) => {
    try {
      const { error, response } = await sessionService.validateUser(req.body);
      if (error) {
        res.status(error).json({ error: response });
      } else {
        res.status(200).json(response);
      }
    } catch (error) {
      res.status(400).json(variables.DEFAULT_ERR);
    }
  })

  // Logout user
  .delete('/logout', async (req, res) => {
    try {
      await sessionService.logOutUser(req.headers.authorization.replace('Bearer ', ''));
      res.status(200).json({ message: 'Successfully logged out!' });
    } catch (error) {
      res.status(400).json(variables.DEFAULT_ERR);
    }
  })

  //forgotten password
  .get('/forgotten', async (req, res) => {
    try {
      const requestId = req.query.ID;
      const { errorCode , response } = await sessionService.createNewPassword(requestId);
      const { error: deleteError } = await sessionService.deletePasswordReq(requestId);
      // cosnt tempalte = getHtmlTemplate(templateName)
      if (errorCode) {
        res.status(errorCode).json({ error: response });
      } else if (deleteError) {
        res.status(errorCode).json({ error: 'Error deleting password request!' });
      } else {
        res.status(200).json({ message: 'Your password has been updated!' });
      }
    } catch (error) {
      res.status(400).json(error);
    }
  })

  // .get('/test', async (req, res) => {
  //   try {
    // res.sendFile(path.resolve('./password-reset.html'));

  //   } catch (error) {
  //     res.status(400).json(error);
  //   }
  // })
 
  //forgotten password request
  .post('/passwordRequest', async (req, res) => {
    try {
      const email = req.body.email;
      const { error, response } = await sessionService.createPasswordReq(email);
      if (error) {
        res.status(error).json({ error: response });
      } else {
        res.status(200).json(response);
      }
    } catch (error) {
      res.status(400).json(error);
    }
  })
  ;

  export default sessionController;