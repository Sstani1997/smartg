import express from 'express';
import variables from '../common/variables.js';
import ordersService from '../services/orders-service.js';
import validateBody from '../middlewares/common/validate-body.js';

const ordersController = express.Router();

ordersController

//View orders
.get('/', async (req, res) => {
    try {
      const { error, response } = await ordersService.getOrders(req.query);
      if (error) {
        res.status(error).json({ error: response });
      } else {
        res.status(200).json(response);
      }
    } catch (error) {
      res.status(400).json(variables.DEFAULT_ERR);
    }
  });


  export default ordersController;