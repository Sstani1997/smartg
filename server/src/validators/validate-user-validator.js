
export default {
  email: (value) => typeof value === 'string' && value.length > 0,
};
