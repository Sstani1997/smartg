
export default {
  firstName: (value) => typeof value === 'string' && value.length > 0,
  lastName: (value) => typeof value === 'string' && value.length > 0,
  email: (value) => typeof value === 'string' && value.length > 5,
  phone: (value) => typeof value === 'string' && value.length === 10,
};
