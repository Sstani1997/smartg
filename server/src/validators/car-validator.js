
export default {
  modelId: (value) => value > 0,
  vin: (value) => typeof value === 'string' &&
    /^(?<wmi>[A-HJ-NPR-Z\d]{3})(?<vds>[A-HJ-NPR-Z\d]{5})(?<check>[\dX])(?<vis>(?<year>[A-HJ-NPR-Z\d])(?<plant>[A-HJ-NPR-Z\d])(?<seq>[A-HJ-NPR-Z\d]{6}))$/.test(value),
  plate: (value) => typeof value === 'string' && /^[a-z]{1,2}[0-9]{4}[a-z]{1,2}$/i.test(value),
  engine: (value) => typeof value === 'string' && value.length > 0,
  transmission: (value) => typeof value === 'string' && value.length > 0,
  year: (value) => typeof value === 'string' && value.length === 4,
  customerId: (value) => value > 0,
};