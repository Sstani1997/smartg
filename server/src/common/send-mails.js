import pool from '../data/pool.js';
import mailjet from 'node-mailjet';

/**
 * Sends email using mailjet service with the given data
 * @author Svetoslav Yordanov <svetlio@gmail.com>
 * @param {string} emailTo Recipient email
 * @param {string} name Recipient name
 * @param {string} subject Email subject
 * @param {string} HTMLEmailBody Email body
 * @returns {void}
 */
export const sendMail = (emailTo, name, subject, HTMLEmailBody) => {
  const mailjetNode = mailjet
    .connect('154efbb766daf8729795402cab015657', 'f229512988578753c6798a78ee7ffed9');
  const request = mailjetNode
    .post('send', { 'version': 'v3.1' })
    .request({
      'Messages': [
        {
          'From': {
            'Email': 'office@smartg.online',
            'Name': 'SmartG Service',
          },
          'To': [
            {
              'Email': emailTo,
              'Name': name,
            },
          ],
          'Subject': subject,
          'HTMLPart': HTMLEmailBody,
        },
      ],
    });
  request
    .then((result) => {
      console.log(result.body);
    })
    .catch((err) => {
      console.log(err.statusCode);
    });

};

/**
 * Sends email with the given email template
 * @author Svetoslav Yordanov <svetlio@gmail.com>
 * @param {object} emailInfo Email data (email, name, html params to be replaced)
 * @param {string} template Template name
 * @returns {void}
 */
export const sendEmailTemplate = async (emailInfo, template) => {
  const sql = `
  SELECT
    concat(t1.html, t2.html, t3.html) as HTML,
    t2.subject
  FROM html_templates t2
  JOIN html_templates t1 on
    t1.name = 'header'
  JOIN html_templates t3 on
    t3.name = 'footer'
  WHERE t2.name = ?`;

  const HTMLTemplate = (await pool.query(sql, [template]))[0];
  const HTMLEmailBody = Object.keys(emailInfo)
    .reduce((acc, e) => {
      return acc.replace(`##${e}##`, emailInfo[e]);
    }, HTMLTemplate.HTML);

  sendMail(emailInfo.email, emailInfo.name, HTMLTemplate.subject, HTMLEmailBody);
};
