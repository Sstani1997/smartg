export default {
  createUser: {
    firstName: 'No first name provided!',
    lastName: 'No last name provided!',
    email: 'No or invalid email provided!',
    phone: 'No or invalid phone provided!',
  },
  validateUser: {
    email: 'No email provided!',
    password: 'No or invalid password provided!',
  },
  validateCar: {
    modelId: 'No model provided!',
    customerId: 'No customer provided!',
    vin: 'No or invalid vin provided!',
    plate: 'No or invalid plate provided!',
    engine: 'No engine provided!',
    transmission: 'No transmission provided!',
    year: 'No or invalid year!',
  },
  validateService: {
    name: 'No name provided!',
    price: 'No or invalid price provided!',
  },
};
