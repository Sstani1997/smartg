import variables from './variables.js';
import SqlString from 'sqlstring';

const sorting = (query, tableFields) => {
  const sort = (tableFields.includes(query.sort))
    ? query.sort : tableFields[0];
  const order = (query.order === 'desc')
    ? 'desc' : 'asc';

  return `ORDER BY ${sort} ${order}, ${tableFields[0]} `;
};

const pagination = (query) => {
  const limitFrom = (query.page > 0)
    ? (query.page - 1) * variables.BOOKS_PER_PAGE : 0;
  const limitTo = variables.ITEMS_PER_PAGE;

  return `LIMIT ${limitFrom}, ${limitTo}`;
};

const returnResult = (result) => {
  if (result.error) {
    return {
      error: result.error,
      response: result.response,
    };
  }

  return { error: null, response: result.response };
};

const isExpired = (date, timeLenght) => {
  return (new Date() - new Date(date)) < timeLenght;
};

const searchQueryParts = (tableFields, query) => {
  const searchQueryParts = query.search?.split(' ') || [];

  const searchFields = searchQueryParts
    .map(part =>
      '(' + tableFields.map(e => `${e} LIKE ${SqlString.escape(`%${part}%`)}`).join(' OR ') + ')')
    .join(' AND ');

  return query.search ? `HAVING ${searchFields}` : '';
};

export default {
  sorting,
  pagination,
  returnResult,
  isExpired,
  searchQueryParts,
};