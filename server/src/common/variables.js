export default {
  ITEMS_PER_PAGE: 40,
  ROLES: {
    user: 'user',
    employee: 'employee',
    admin: 'admin',
  },
  DEFAULT_ERR: { error: 'Something went wrong! Our developers work on it!' },
};
