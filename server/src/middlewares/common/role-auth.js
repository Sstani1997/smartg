export default (role) => async (req, res, next) => {
  if (req.user.role !== role) {
    return res.status(403).json({ error: 'You are not authorized!'});
  }

  await next();
};
