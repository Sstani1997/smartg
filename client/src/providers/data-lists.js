import { BASE_URL } from '../common/variables';

export const getBrandsModels = async () => {
  const result = await fetch(`${BASE_URL}/cars/models`, {
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  })
    .then((response) => response.json())
    .catch(console.warn);

    return result;
}

export const getUsers = async () => {
  const result = await fetch(`${BASE_URL}/users/list`, {
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  })
    .then((response) => response.json())
    .catch(console.warn);

    return result;
}