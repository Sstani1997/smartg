import Footer from './components/Base/Footer/Footer';
import Header from './components/Base/Header/Header';
import Home from './components/Pages/Home/Home';
import NotFound from './components/Pages/NotFound/NotFound';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { useState } from 'react';
import AuthContext, { getUser } from './providers/authContext';
import GuardedRoute from './components/HOC/GuardedRoute';
import Login from './components/Login/Login';
import { HelmetProvider } from 'react-helmet-async';
import NewOrder from './components/Orders/NewOrder';
import AllCars from './components/Cars/AllCars';
import EditCar from './components/Cars/EditCar';
import AllCustomers from './components/Customers/AllCustomers';
import AllServices from './components/Service/AllServices';
import EditService from './components/Service/EditService';
import Register from './components/Register/Register';
import ForgotPassword from './components/Pages/Forgotten password/Forgotten password';
import PasswordReset from './components/Pages/Forgotten password/Password reset';
import AllOrders from './components/Orders/Orders';

const App = () => {
  // const root = document.documentElement;
  // root.style.setProperty('--site-dark-background', Math.random() > 0.5 ? '#821212' : '#796b09');
  const [authState, setAuthState] = useState({
    user: getUser(),
    isLoggedIn: Boolean(getUser()),
  })
  // console.log(useLocation())
  return (
    <>
      <HelmetProvider>
        <BrowserRouter>
          <AuthContext.Provider value={{ ...authState, setAuthState }}>
            <Header />
            <div className="site">
              <div className="site-content" id="container">
                <Switch>
                  <Route exact path="/" component={Home} />
                  {/* <GuardedRoute path="/:lang/books/:bookId" isLoggedIn={authState.isLoggedIn} component={SingleBook} /> */}
                  <GuardedRoute path="/orders/new" isLoggedIn={authState.isLoggedIn} component={NewOrder} />
                  <GuardedRoute path="/orders" isLoggedIn={authState.isLoggedIn} component={AllOrders} />
                  <GuardedRoute exact path="/customers" isLoggedIn={authState.isLoggedIn} component={AllCustomers} />
                  <GuardedRoute exact path="/cars" isLoggedIn={authState.isLoggedIn} component={AllCars} />
                  <GuardedRoute exact path="/services" isLoggedIn={authState.isLoggedIn} component={AllServices} />
                  <GuardedRoute exact path="/cars/:carId" isLoggedIn={authState.isLoggedIn} component={EditCar} />
                  <GuardedRoute exact path="/services/:serviceId" isLoggedIn={authState.isLoggedIn} component={EditService} />
                  <Route exact path="/reset" component={ForgotPassword} />
                  <Route exact path="/reset/success" component={PasswordReset} />
                  <Route path="/login" component={Login} />
                  <Route path="/register" component={Register} />
                  <Route path="*" component={NotFound} />
                </Switch>
              </div>
            </div>
            <Footer />
          </AuthContext.Provider>
        </BrowserRouter>
      </HelmetProvider>
    </>
  );
}

export default App;
