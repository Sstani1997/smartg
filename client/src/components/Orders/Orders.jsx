import { useEffect, useState } from 'react';
import { Dropdown } from 'react-bootstrap';
import { useLocation } from 'react-router';
import { Link } from 'react-router-dom';
import { BASE_URL } from '../../common/variables';
import ManageOrder from '../Orders/ManageOrder';
import CustomModal from '../HOC/CustomModal';

const AllOrders = () => {

  const [customersList, setOrdersList] = useState([]);
  const [uniqueOrders, setUniqueOrders] = useState([]);
  const [orderData, setOrderData] = useState(null);
  const [manageAction, setManageAction] = useState('');


  const useCustomQueryParams = Object.fromEntries(new URLSearchParams(useLocation().search));
  const [filters, setFilters] = useState(useCustomQueryParams);
  const updateFilters = (newFilter) => {
    if (filters['sort'] === newFilter['sort']) {
      filters['order'] = filters['order'] === 'desc' ? 'asc' : 'desc';
    } else {
      filters['order'] = 'asc';
    }
    setFilters({ ...filters, ...newFilter });
  };


  useEffect(() => {
    !filters['sort'] && setFilters({ ...filters, sort: 'fullName', order: 'asc' });

    const query = Object.keys(filters)
      .filter(e => Boolean(filters[e]))
      .map((e) => `${e}=${filters[e]}`)
      .join('&');
    fetch(`${BASE_URL}/orders?${query}`, {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then(response => {
        setOrdersList(response);
      });
  }, [filters])

  const setOrderClass = (col) => {
    return filters['sort'] === col ? filters['order'] : '';
  }

  const [show, setShow] = useState(false);

  const handleUser = (action, customer) => {
    let newList = [...uniqueOrders];
    if (action === 'edit') {
      const updatedIndex = uniqueOrders.findIndex(e => e.id === customer.id);
      newList[updatedIndex] = { ...newList[updatedIndex], ...customer };
    } else {
      newList = [customer, ...uniqueOrders];
    }
    setUniqueOrders([...newList]);
    setShow(false);
  };

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const manageCustomer = (action, order = {}) => {
    setManageAction(action);
    setOrderData(order);
    handleShow();
  }

  useEffect(() => {
    const uniqueOrders = [
      ...new Map(customersList
        .map(item =>
          [item['id'], item])).values()
    ];
    setUniqueOrders([...uniqueOrders]);
  }, [customersList]);

  return (
    <>
      <h1>Orders list&nbsp;&nbsp;&nbsp;<input style={{ width: '70%' }} type="text" placeholder="Search by every column ..." onChange={(e) => updateFilters({ search: e.target.value })} /></h1>
      {/* from: <input type="text" name="from" style={{ width: '160px' }} onChange={(e) => updateFilters({ from: e.target.value })} />&nbsp;&nbsp;
      to: <input type="text" size="20" name="to" style={{ width: '160px' }} onChange={(e) => updateFilters({ to: e.target.value })} /> */}
      <button style={{ margin: "1em" }} onClick={() => manageCustomer('create')}>New order</button>
      <div className="content-box">
        <table className="list-table">
          <thead>
            <tr>
              <th className={setOrderClass('fullName')}>
                <Link to="#" onClick={() => updateFilters({ sort: 'fullName' })} >Customer </Link>
              </th>
              <th className={setOrderClass('phone')}>
                <Link to="#" onClick={() => updateFilters({ sort: 'phone' })} >Phone number </Link>
              </th>
              <th className={setOrderClass('email')}>
                <Link to="#" onClick={() => updateFilters({ sort: 'email' })} >Email </Link>
              </th>
              <th className={setOrderClass('address')}>
                <Link to="#" onClick={() => updateFilters({ sort: 'address' })} >Address </Link>
              </th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {uniqueOrders.map((customer) => {
              return (
                <>
                  <tr key={customer.id}>
                    <td className="brand">{customer.firstName} {customer.lastName}</td>
                    <td >{customer.phone}</td>
                    <td>{customer.email}</td>
                    <td><span >{customer.address}</span></td>
                    <td>
                      <Dropdown>
                        <Dropdown.Toggle variant="default" id="dropdown-basic">Action</Dropdown.Toggle>
                        <Dropdown.Menu>
                          <Dropdown.Item onClick={() => manageCustomer('edit', customer)}>Edit</Dropdown.Item>
                          {/* <Dropdown.Item onClick={() => changePassword(customer)}>Edit</Dropdown.Item> */}
                        </Dropdown.Menu>
                      </Dropdown>
                    </td>
                  </tr>
                </>
              )
            })
            }
            {!customersList.length && <tr><td colSpan="8"><h2>No customers found with the search criteria... </h2></td></tr>}
          </tbody>
        </table>
      </div>
      <CustomModal
        show={show}
        handleClose={handleClose}>

        <ManageOrder
          action={manageAction}
          userData={orderData}
          handleUser={handleUser}
          handleClose={handleClose} />

      </CustomModal>
    </>
  )
}
export default AllOrders;