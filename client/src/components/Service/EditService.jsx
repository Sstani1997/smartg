import { useForm } from 'react-hook-form';
import { Alert } from 'react-bootstrap';
import { BASE_URL } from '../../common/variables';
import { useEffect, useState } from 'react';

const EditService = ({ serviceData, handleUpdate, handleClose, handelDelete }) => {
  const [message, setMessage] = useState([]);
  
  

  const updateService = (service) => {
    fetch(`${BASE_URL}/services/${serviceData.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(service),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          const errorMessage = typeof result.error === 'object' ? Object.values(result.error) : result.error;
          throw errorMessage;
        }
        setMessage(['success', ['Service info was successfully updated!']]);
        setTimeout(() => {
          setMessage([]);
          handleUpdate(service)
        }, 2000);
      })
      .catch((error) => {
        setMessage(['danger', error]);
      });
  }

  const { setValue, register, handleSubmit, formState: { errors: formErrors } } = useForm({ mode: 'onSubmit' });
  useEffect(() => {
    setValue("name", serviceData.name);
    setValue("price", serviceData.price);
    setValue("id", serviceData.id);

  }, [serviceData]);
  return (
    <>
      <h1>Edit service: <span style={{ color: 'black' }}>{serviceData.fullName}</span></h1>
      <div className="contentBox">
        <div className="register">
          {message[1] &&
            <Alert variant={message[0]}>
              {message[1]}
            </Alert>
          }
          <form onSubmit={handleSubmit(updateService)}>
          {/* <form onSubmit={handleSubmit(deleteService)}> */}
            <input type="hidden" {...register('id')} />
            <label htmlFor="name">Service name *: </label>
            <input
              {...register('name', { required: 'Service name is required!' })}
              type="text"
              placeholder="Service name..."
              className={formErrors.name && 'error'}
            />
            {formErrors.name && <div className="error">{formErrors.name.message}</div>}
            <br />
            <br />

            <label htmlFor="price">Price*:&nbsp;</label>
            <input
              type="text"
              {...register('price', {
                required: 'price is required!', pattern: {
                  value: /^[0-9]{1,3}$/i,
                  message: "Invalid price"
                }
              })}
              placeholder="Price..."
            />
            {formErrors.price && <div className="error">{formErrors.price.message}</div>}
        <div className="edit-service-buttons">
            <button type="button" onClick={handleClose} >Cancel</button>
            <button type="submit" >Save</button>
        </div>
            
          </form>
        </div>
      </div>

    </>
  )
}

export default EditService;