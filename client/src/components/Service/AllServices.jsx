import { useEffect, useState } from 'react';
import { Alert, Modal } from 'react-bootstrap';
import { useLocation } from 'react-router';
import { Link } from 'react-router-dom';
import { BASE_URL } from '../../common/variables';
import EditService from './EditService';

const AllServices = () => {

  const [serviceList, setServiceList] = useState([]);
  const [serviceData, setServiceData] = useState(null);
  const [show, setShow] = useState(false);
  const [message, setMessage] = useState([]);

  const handleUpdate = (service) => {
    const updatedIndex = serviceList.findIndex(e => e.id === service.id);
    serviceList[updatedIndex] = { ...serviceList[updatedIndex], ...service };
    setServiceList([...serviceList]);
    setShow(false);
  };

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  function OpenEditService(service) {
    setServiceData(service);
    handleShow();
  }
  const useCustomQueryParams = Object.fromEntries(new URLSearchParams(useLocation().search));
  const [filters, setFilters] = useState(useCustomQueryParams);
  const updateFilters = (newFilter) => {
    if (filters['sort'] === newFilter['sort']) {
      filters['order'] = filters['order'] === 'desc' ? 'asc' : 'desc'
    } else {
      filters['order'] = 'asc';
    }
    setFilters({ ...filters, ...newFilter });
  };


  // props.history.push(`/cars?${query}`)
  const deleteService = (id) => {
    fetch(`${BASE_URL}/services/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: ` Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          const errorMessage = typeof result.error === 'object' ? Object.values(result.error) : result.error;
          throw errorMessage;
        }
        setServiceList(data => data.filter(e => e.id !== id));

        setMessage(['success', ['Service info was deleted successfully!']]);
        setTimeout(() => {
          setMessage([]);
        }, 2000);
      })
      .catch((error) => {
        setMessage(['danger', error]);
      });
  }

  useEffect(() => {
    !filters['sort'] && setFilters({ ...filters, sort: 'parentName', order: 'asc' });

    const query = Object.keys(filters)
      .filter(e => Boolean(filters[e]))
      .map((e) => `${e}=${filters[e]}`)
      .join('&');
    fetch(`${BASE_URL}/services?${query}`, {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then(response => {
        setServiceList(response);
      });
  }, [filters])

  const setOrderClass = (col) => {
    return filters['sort'] === col ? filters['order'] : '';
  }

  return (
    <>
      <h1>Service list&nbsp;&nbsp;&nbsp;<input type="text" onChange={(e) => updateFilters({ search: e.target.value })} /></h1>
      <h6>
        Price From:<input style={{ width: '15%' }} type="text" placeholder="From"
          onChange={(e) => updateFilters({ from: e.target.value })} />
          &nbsp;
       To: <input style={{ width: '15%' }} type="text" placeholder="To"
          onChange={(e) => updateFilters({ to: e.target.value })} />
      </h6>
      <div className="content-box">
        {message[1] &&
          <Alert variant={message[0]}>
            {message[1]}
          </Alert>
        }
        <table className="list-table">
          <thead>
            <tr>
              <th className={setOrderClass('name')}>
                <Link to="#" onClick={() => updateFilters({ sort: 'name' })} >Name </Link>
              </th>
              <th className={setOrderClass('price')}>
                <Link to="#" onClick={() => updateFilters({ sort: 'price' })} >Price </Link>
              </th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {serviceList.map((service) => {
              return (
                <tr key={service.id}>
                  <td className="brand">
                    <strong>{service.parentName}</strong>&nbsp;{service.name}
                  </td>
                  <td>{service.price}</td>
                  <td><button onClick={() => OpenEditService(service)}>Edit</button>
                    <button type="button" onClick={(e) => { if (window.confirm('Are you sure you wish to delete this item?')) deleteService(service.id) }} >Delete service</button>
                    {/* onClick={(e) => { if (window.confirm('Are you sure you wish to delete this item?')) this.deleteItem(e) }  */}
                  </td>
                  {/* history.push(`/services/${service.id}`) */}
                </tr>
              )
            })
            }
            {!serviceList.length && <tr><td colSpan="3"><h2>No services found with the search criteria... </h2></td></tr>}
          </tbody>
        </table>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Body>
          <EditService
            serviceData={serviceData}
            handleUpdate={handleUpdate}
            handleClose={handleClose} />
        </Modal.Body>
      </Modal>
    </>
  )
}
export default AllServices;