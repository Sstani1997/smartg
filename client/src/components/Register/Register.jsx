import { useState } from 'react';
import { BASE_URL } from '../../common/variables';
import { useForm } from 'react-hook-form';
import { Helmet } from 'react-helmet-async';
import { Alert } from 'react-bootstrap';

const Register = ({ history }) => {
  const [errors, setErrors] = useState('');
  // const [registered, setRegistered] = useState({});
  const registerUser = (user) => {
    fetch(`${BASE_URL}/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(user),
    })
      .then(r => r.json())
      .then(({ error }) => {
        if (error) {
          const errorMessage = typeof error === 'object' ? Object.values(error).join('\n') : error;
          setErrors(errorMessage.toString());
        }
        // setRegistered(user)
        history.push('./login');
      })
      .catch(error => setErrors(error.toString()));
  };

  const { register, handleSubmit, formState: { errors: formErrors, isValid } } = useForm({ mode: "onChange" });
  return (
    <>

      <Helmet>
        <title>Register | Home</title>
      </Helmet>
      <h1>Register</h1>
      <div className="register">
        {errors &&
          <Alert variant={'danger'}>
            {errors}
          </Alert>
        }
        <form onSubmit={handleSubmit(registerUser)}>
          <label htmlFor="firstName">First name*:&nbsp;</label>
          <input
            {...register('firstName', { required: 'First name is required!' })}
            type="text"
            placeholder="First name..."
            className={formErrors.firstName && 'error'}
          />
          {formErrors.firstName && <div className="error">{formErrors.firstName.message}</div>}
          <br />
          <label htmlFor="lastName">Last name*:&nbsp;</label>
          <input
            type="text"
            {...register('lastName', { required: 'Last name is required!' })}
            placeholder="Last name..."
          />
          {formErrors.lastName && <div className="error">{formErrors.lastName.message}</div>}
          <label htmlFor="email">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E-mail*:&nbsp;</label>
          <input
            type="text"
            {...register('email', {
              required: 'Email is required!', pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                message: "Invalid email address"
              }
            })}
            placeholder="E-mail..."
          />
          {formErrors.email && <div className="error">{formErrors.email.message}</div>}
          <br />
          <br />
          {/* <button onClick={registerUser}>Register</button> */}
          <button disabled={!isValid} type="submit">Register</button>
        </form>
      </div>
    </>
  );
};

export default Register;
