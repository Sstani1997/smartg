import { useEffect, useState } from 'react';
import { useLocation } from 'react-router';
import { Link } from 'react-router-dom';
import { BASE_URL } from '../../common/variables';
import CustomModal from '../HOC/CustomModal';
import ManageCar from './ManageCar';

const AllCars = ({history}) => {

  const [carsList, setCarsList] = useState([]);
  const [show, setShow] = useState(false);
  const [carData, setCarData] = useState(null);
  const [manageAction, setManageAction] = useState('');


  const useCustomQueryParams = Object.fromEntries(new URLSearchParams(useLocation().search));
  const [filters, setFilters] = useState(useCustomQueryParams);
  const updateFilters = (newFilter) => {
    if (filters['sort'] === newFilter['sort']) {
      filters['order'] = filters['order'] === 'desc' ? 'asc' : 'desc'
    } else {
      filters['order'] = 'asc';
    }
    setFilters({ ...filters, ...newFilter });
  };


  // props.history.push(`/cars?${query}`)

  useEffect(() => {
    !filters['sort'] && setFilters({ ...filters, sort: 'brand', order: 'asc' });

    const query = Object.keys(filters)
      .filter(e => Boolean(filters[e]))
      .map((e) => `${e}=${filters[e]}`)
      .join('&');
    fetch(`${BASE_URL}/cars?${query}`, {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then(response => {
        setCarsList(response);
      });
  }, [filters])

  const setOrderClass = (col) => {
    return filters['sort'] === col ? filters['order'] : '';
  }

  const handleCar = (action, car) => {
    console.log(car);
    let newList = [...carsList];
    if (action === 'edit') {
      const updatedIndex = carsList.findIndex(e => e.id === car.id);
      newList[updatedIndex] = { ...newList[updatedIndex], ...car };
    } else {
      newList = [car, ...carsList];
    }
    setCarsList([...newList]);
    setShow(false);
  };

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const manageCar = (action, car = {}) => {
    setManageAction(action);
    setCarData(car);
    handleShow();
  }

  return (
    <>
      <h1>Vehicles list&nbsp;&nbsp;&nbsp;<input type="text" onChange={(e) => updateFilters({ search: e.target.value })} /></h1>
      <button style={{ margin: "1em" }} onClick={() => manageCar('create')}>New vehicle</button>
      <div className="content-box">
        <table className="list-table">
          <thead>
            <tr>
              <th className={setOrderClass('brand')}>
                <Link to="#" onClick={() => updateFilters({ sort: 'brand' })} >Brand </Link>
              </th>
              <th className={setOrderClass('model')}>
                <Link to="#" onClick={() => updateFilters({ sort: 'model' })} >Model </Link>
              </th>
              <th className={setOrderClass('year')}>
                <Link to="#" onClick={() => updateFilters({ sort: 'year' })} >Year </Link>
              </th>
              <th className={setOrderClass('transmission')}>
                <Link to="#" onClick={() => updateFilters({ sort: 'transmission' })} >Transmission </Link>
              </th>
              <th className={setOrderClass('vin')}>
                <Link to="#" onClick={() => updateFilters({ sort: 'vin' })} >Vin </Link>
              </th>
              <th className={setOrderClass('plate')}>
                <Link to="#" onClick={() => updateFilters({ sort: 'plate' })} >Plate </Link>
              </th>
              <th className={setOrderClass('fullName')}>
                <Link to="#" onClick={() => updateFilters({ sort: 'fullName' })} >Owner </Link>
              </th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {carsList.map((car) => {
              return (
                <tr key={car.id}>
                  <td className="brand">
                    {/* <img src={car.logoURL} width="64" align="left" alt={car.brand} /> */}
                    {car.brand}
                  </td>
                  <td>{car.model}</td>
                  <td>{car.year}</td>
                  <td>{car.transmission}</td>
                  <td>{car.vin}</td>
                  <td><span className="car-plate">{car.plate}</span></td>
                  <td>{car.fullName}</td>
                  <td><button onClick={() => manageCar('edit', car)}>Edit</button></td>
                </tr>
              )
            })
            }
            {!carsList.length && <tr><td colSpan="8"><h2>No vehicles found with the search criteria... </h2></td></tr>}
          </tbody>
        </table>
      </div>
      <CustomModal
        show={show}
        handleClose={handleClose}>

        <ManageCar
          action={manageAction}
          carData={carData}
          handleCar={handleCar}
          handleClose={handleClose} />

      </CustomModal>
    </>
  )
}
export default AllCars;