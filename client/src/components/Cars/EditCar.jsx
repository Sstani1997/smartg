import { useForm, Controller } from 'react-hook-form';
import { Alert } from 'react-bootstrap';
import { BASE_URL } from '../../common/variables';
import { useEffect, useState } from 'react';
import Select from 'react-select';
import { getBrandsModels, getUsers } from '../../providers/data-lists';

const EditCar = ({ match, history }) => {
  const [message, setMessage] = useState([]);
  const [brandsModels, setBrandsModels] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [carData, setCarData] = useState({});
  const [selectedModel, setSelectedModel] = useState(null);
  const [selectedCustomer, setSelectedCustomer] = useState(null);

  useEffect(() => {
    getBrandsModels().then(setBrandsModels);
    getUsers().then(setCustomers);
  }, []);

  useEffect(() => {
    fetch(`${BASE_URL}/cars/${match.params.carId}`, {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          history.push(`/notfound`)
        }
        setCarData(result);
        setSelectedModel(result.modelId);
        setSelectedCustomer(result.customerId);
      })
      .catch((error) => {
        setMessage(['danger', error.message]);
      });


  }, [match.params.carId, history]);


  const createCar = (car) => {
    fetch(`${BASE_URL}/cars/${match.params.carId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        ...car,
        modelId: car.modelId.id,
        customerId: car.customerId.id
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          const errorMessage = typeof result.error === 'object' ? Object.values(result.error) : result.error;
          throw errorMessage;
        }
        setMessage(['success', ['Vehicle was successfully updated!']]);
        setTimeout(() => {
          setMessage([]);
          history.push('/cars');
        }, 3000);
      })
      .catch((error) => {
        setMessage(['danger', error]);
      });
  }

  const updateCar = (car) => {
    fetch(`${BASE_URL}/cars/${match.params.carId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        ...car,
        modelId: car.modelId.value,
        customerId: car.customerId.id
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          const errorMessage = typeof result.error === 'object' ? Object.values(result.error) : result.error;
          throw errorMessage;
        }
        setMessage(['success', ['Vehicle was successfully updated!']]);
        setTimeout(() => {
          setMessage([]);
          history.push('/cars');
        }, 3000);
      })
      .catch((error) => {
        setMessage(['danger', error]);
      });
  }

  const { setValue, control, register, handleSubmit, formState: { errors: formErrors } } = useForm({ mode: 'onSubmit' });

  useEffect(() => {
    setValue("year", carData.year);
    setValue("plate", carData.plate);
    setValue("vin", carData.vin);
    setValue("engine", carData.engine);
    setValue("transmission", carData.transmission);
    setValue("modelId", brandsModels.find(e => e.value === carData.modelId));
    setValue("customerId", customers.find(e => e.id === carData.customerId));
  }, [carData, brandsModels, setValue, customers]);

  const setSelected = (field, fn, value) => {
    setValue(field, value);
    fn(value.value);
  }

  return (
    <>
      <h1>Edit vehicle: <span style={{ color: 'black' }}>{carData.brand} {carData.model} - {carData.plate}</span></h1>
      <div className="contentBox">
        <div className="register">
          {message[1] &&
            <Alert variant={message[0]}>
              {message[1].map(e => <div key={e}>{e}</div>)}
            </Alert>
          }
          <form onSubmit={handleSubmit(updateCar)}>
            <label htmlFor="customerId">Customer*: </label>
            <Controller
              name="customerId"
              control={control}
              value={customers.find(e => e.value === selectedCustomer)}
              render={() => (
                <Select
                  {...register('customerId', {
                    required: 'Customer is required!'
                  })}
                  onChange={(e) => setSelected('customerId', setSelectedCustomer, e)}
                  options={customers}
                  getOptionValue={option => option.id}
                  getOptionLabel={option => option.fullName}
                  defaultValue={customers.find(e => e.id === selectedCustomer)}
                  value={customers.find(e => e.id === selectedCustomer)}
                  isSearchable
                />
              )}
            />
            {formErrors.customerId && <div className="error">{formErrors.customerId.message}</div>}
            <br />

            <label htmlFor="modelId">Vehicle model*: </label>
            <Controller
              name="modelId"
              control={control}
              render={() => (
                <Select
                  {...register('modelId', {
                    required: 'Brand > Model is required!'
                  })}
                  options={brandsModels}
                  isSearchable
                  onChange={(e) => setSelected('modelId', setSelectedModel, e)}
                  value={brandsModels.find(e => e.value === selectedModel)}
                />
              )}
            />
            {formErrors.modelId && <div className="error">{formErrors.modelId.message}</div>}
            <br />

            <label htmlFor="year">Production year*: </label>
            <input
              {...register('year', {
                required: 'Production year is required!', pattern: {
                  value: /^[0-9]{4}$/,
                  message: "Invalid year!"
                }
              })}
              type="text"
              placeholder="Production year..."
              className={formErrors.year && 'error'}
            />
            {formErrors.year && <div className="error">{formErrors.year.message}</div>}

            <br />
            <br />
            <label htmlFor="plate">Reg. plate number*:&nbsp;</label>
            <input
              style={{ textTransform: 'uppercase' }}
              type="text"
              {...register('plate', {
                required: 'Reg. plate number is required!', pattern: {
                  value: /^[a-z]{1,2}[0-9]{4}[a-z]{1,2}$/i,
                  message: "Invalid plate number!"
                }
              })}
              placeholder="Reg. plate number..."
            />
            {formErrors.plate && <div className="error">{formErrors.plate.message}</div>}

            <br />
            <br />
            <label htmlFor="vin">VIN*:&nbsp;</label>
            <input
              type="text"
              {...register('vin', {
                required: 'VIN is required!', pattern: {
                  value: /^(?<wmi>[A-HJ-NPR-Z\d]{3})(?<vds>[A-HJ-NPR-Z\d]{5})(?<check>[\dX])(?<vis>(?<year>[A-HJ-NPR-Z\d])(?<plant>[A-HJ-NPR-Z\d])(?<seq>[A-HJ-NPR-Z\d]{6}))$/i,
                  message: "Invalid VIN number!"
                }
              })}
              placeholder="VIN..."
            />
            {formErrors.vin && <div className="error">{formErrors.vin.message}</div>}
            <br />
            <br />
            <label htmlFor="engine">Engine*:&nbsp;</label>
            <input
              type="text"
              {...register('engine', {
                required: 'Engine is required!'
              })}
              placeholder="Engine..."
            />
            {formErrors.engine && <div className="error">{formErrors.engine.message}</div>}
            <br />
            <br />
            <label htmlFor="transmission">Transmission*:&nbsp;</label>
            <input
              type="text"
              {...register('transmission', {
                required: 'Transmission is required!'
              })}
              placeholder="Transmission..."
            />
            {formErrors.transmission && <div className="error">{formErrors.transmission.message}</div>}
            <br />
            <br />
            <br />
            {/* <button onClick={registerUser}>Register</button> */}
            <button type="submit">Save</button>
          </form>
          {message[1] &&
            <Alert variant={message[0]} style={{ margin: '1em auto' }}>
              {message[1].map(e => <div key={e}>{e}</div>)}
            </Alert>
          }
        </div>
      </div>

    </>
  )
}

export default EditCar;