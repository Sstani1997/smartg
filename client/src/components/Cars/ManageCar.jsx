import { Controller, useForm } from 'react-hook-form';
import { Alert } from 'react-bootstrap';
import { BASE_URL } from '../../common/variables';
import { useEffect, useState } from 'react';
import { getBrandsModels, getUsers } from '../../providers/data-lists';
import Select from 'react-select';


const ManageCar = ({ action, carData, handleCar, handleClose }) => {
  const [message, setMessage] = useState([]);
  const [brandsModels, setBrandsModels] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [selectedModel, setSelectedModel] = useState(carData.modelId);
  const [selectedCustomer, setSelectedCustomer] = useState(carData.customerId);

  useEffect(() => {
    getBrandsModels().then(setBrandsModels);
    getUsers().then(setCustomers);
  }, []);

  const handleManage = (data) => {
    action === 'edit' ? updateCar(data) : createCar(data);

  }

  const createCar = (car) => {
    fetch(`${BASE_URL}/cars`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(car),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          const errorMessage = typeof result.error === 'object' ? Object.values(result.error) : result.error;
          throw errorMessage;
        }
        setMessage(['success', ['Vehicle was successfully created!']]);
        setTimeout(() => {
          setMessage([]);
          handleCar(action, result.response)
        }, 2000);
      })
      .catch((error) => {
        setMessage(['danger', [error.message]]);
      });
  }

  const updateCar = (car) => {

    fetch(`${BASE_URL}/cars/${carData.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        ...car,
        modelId: car.modelId.value,
        customerId: car.customerId.id
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          const errorMessage = typeof result.error === 'object' ? Object.values(result.error) : result.error;
          throw errorMessage;
        }
        setMessage(['success', ['Vehicle info was successfully updated!']]);
        setTimeout(() => {
          setMessage([]);
          handleCar(action, {
            ...car,
            id: carData.id,
            modelId: car.modelId.value,
            model: car.modelId.label,
            customerId: car.customerId.id
          })
        }, 2000);
      })
      .catch((error) => {
        setMessage(['danger', [error]]);
      });
  }

  const { setValue, control, register, handleSubmit, formState: { errors: formErrors } } = useForm({ mode: 'onSubmit' });
  useEffect(() => {
    setValue("year", carData.year);
    setValue("plate", carData.plate);
    setValue("vin", carData.vin);
    setValue("engine", carData.engine);
    setValue("transmission", carData.transmission);
    setValue("modelId", brandsModels.find(e => e.value === carData.modelId));
    setValue("customerId", customers.find(e => e.id === carData.customerId));
  }, [carData, brandsModels, setValue, customers]);


  const setSelected = (field, fn, value) => {
    setValue(field, value.id);
    fn(value.value);
  }

  return (
    <>
      {action === 'edit' ? <h1>Edit vehicle: <span style={{ color: 'black' }}>{carData.brand} {carData.model} - {carData.plate}</span></h1> :
        <h1>Create vehicle</h1>}
      <div className="contentBox">
        <div className="register">
          {message[1] &&
            <Alert variant={message[0]}>
              {message[1].map(e => <div key={e}>{e}</div>)}
            </Alert>
          }
          <form onSubmit={handleSubmit(handleManage)}>
            <label htmlFor="customerId">Customer*: </label>
            <Controller
              name="customerId"
              control={control}
              value={customers.find(e => e.value === selectedCustomer)}
              render={() => (
                <Select
                  {...register('customerId', {
                    required: 'Customer is required!'
                  })}
                  onChange={(e) => setSelected('customerId', setSelectedCustomer, e)}
                  options={customers}
                  getOptionValue={option => option.id}
                  getOptionLabel={option => option.fullName}
                  defaultValue={customers.find(e => e.id === selectedCustomer)}
                  value={customers.find(e => e.id === selectedCustomer)}
                  isSearchable
                />
              )}
            />
            {formErrors.customerId && <div className="error">{formErrors.customerId.message}</div>}
            <br />

            <label htmlFor="modelId">Vehicle model*: </label>
            <Controller
              name="modelId"
              control={control}
              render={() => (
                <Select
                  {...register('modelId', {
                    required: 'Brand > Model is required!'
                  })}
                  options={brandsModels}
                  isSearchable
                  onChange={(e) => setSelected('modelId', setSelectedModel, e)}
                  value={brandsModels.find(e => e.value === selectedModel)}
                />
              )}
            />
            {formErrors.modelId && <div className="error">{formErrors.modelId.message}</div>}
            <br />

            <input
              {...register('year', {
                required: 'Production year is required!', pattern: {
                  value: /^[0-9]{4}$/,
                  message: "Invalid year!"
                }
              })}
              type="text"
              placeholder="Production year..."
              className={formErrors.year && 'error'}
            />
            {formErrors.year && <div className="error">{formErrors.year.message}</div>}

            <br />
            <br />
            <input
              style={{ textTransform: 'uppercase' }}
              type="text"
              {...register('plate', {
                required: 'Reg. plate number is required!', pattern: {
                  value: /^[a-z]{1,2}[0-9]{4}[a-z]{1,2}$/i,
                  message: "Invalid plate number!"
                }
              })}
              placeholder="Reg. plate number..."
            />
            {formErrors.plate && <div className="error">{formErrors.plate.message}</div>}

            <br />
            <br />
            <input
              type="text"
              {...register('vin', {
                required: 'VIN is required!', pattern: {
                  value: /^(?<wmi>[A-HJ-NPR-Z\d]{3})(?<vds>[A-HJ-NPR-Z\d]{5})(?<check>[\dX])(?<vis>(?<year>[A-HJ-NPR-Z\d])(?<plant>[A-HJ-NPR-Z\d])(?<seq>[A-HJ-NPR-Z\d]{6}))$/i,
                  message: "Invalid VIN number!"
                }
              })}
              placeholder="VIN number..."
            />
            {formErrors.vin && <div className="error">{formErrors.vin.message}</div>}
            <br />
            <br />
            <input
              type="text"
              {...register('engine', {
                required: 'Engine is required!'
              })}
              placeholder="Engine type..."
            />
            {formErrors.engine && <div className="error">{formErrors.engine.message}</div>}
            <br />
            <br />
            <input
              type="text"
              {...register('transmission', {
                required: 'Transmission is required!'
              })}
              placeholder="Transmission type..."
            />
            {formErrors.transmission && <div className="error">{formErrors.transmission.message}</div>}
            <br />
            <br />
            <br />
            <button type="button" onClick={handleClose} style={{ width: '40%' }}>Cancel</button>&nbsp;<button type="submit" style={{ width: '40%' }}>Save</button>
          </form>
          {message[1] &&
            <Alert variant={message[0]} style={{ margin: '1em auto' }}>
              {message[1].map(e => <div key={e}>{e}</div>)}
            </Alert>
          }
        </div>
      </div>

    </>
  )
}

export default ManageCar;