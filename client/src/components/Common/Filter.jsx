import { useState } from "react";
import ListBox from "./ListBox";
import authorsData from "../../data/authors.data";
import genresData from "../../data/genres-data";
import { useLocation, withRouter } from 'react-router';
import translations from '../../translations/navigation';
const Filter = ({ filterBooks, lang }) => {
  const useCustomQueryParams = Object.fromEntries(new URLSearchParams(useLocation().search));
  const [filters, setFilters] = useState(useCustomQueryParams);
  const updateFilters = (newFilter) => {
    setFilters({ ...filters, ...newFilter });
  };
  // console.log(query);
  // console.log(authorsData(lang));
  return (
    <div className="filter-list">
      {translations.freeBooks[lang]}:&nbsp;&nbsp;
      <input type="checkbox" field="is_borrowed" value="0" onChange={(e) => updateFilters({ is_borrowed: e.target.checked ? 'free' : null })} />&nbsp;&nbsp;&nbsp;&nbsp;
      {translations.author[lang]}:
      <ListBox
        data={authorsData}
        updateFilters={updateFilters}
        field="authorId"
        table="authors"
        lang={lang}
        selected={filters['authorId']}
      />
      {translations.genre[lang]}:
      <ListBox
        data={genresData}
        updateFilters={updateFilters}
        field="genreId"
        table="genres"
        lang={lang}
        selected={filters['genreId']}
      />
      <input type="text" name="search" placeholder="Search..." onChange={(e)=>updateFilters({search:e.target.value})}/>
      <button onClick={() => filterBooks(filters)}>{translations.search[lang]}</button>
    </div>
  );
};

export default withRouter(Filter);
