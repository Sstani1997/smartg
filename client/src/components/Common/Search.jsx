const Search = () => {
  return (
    <div id="search-nav">
    <div>
      <input id="search" type="text" className="site-search" placeholder="Search here..." />
    </div>
  </div>
  );
};

export default Search;