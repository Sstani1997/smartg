import { useState } from 'react';

const ListBox = ({
  data,
  field,
  value
}) => {
  const [selected, setSelected] = useState(value || 0)

  return (
    <select name={field} value={selected} onChange={(e) => setSelected(e.target.value)}>
      <option key={null} value={false}>All...</option>
      {data.map((e) => {
        return (
          <option key={e.id} value={e.id} >
            {e.value}
          </option>
        )
      })
      }
    </select>
  );
};

export default ListBox;
