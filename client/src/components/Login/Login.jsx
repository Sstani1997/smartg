import { useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import decode from 'jwt-decode';
import { BASE_URL } from '../../common/variables';
import AuthContext from '../../providers/authContext';
import { useForm } from 'react-hook-form';
import { Helmet } from 'react-helmet-async';
import { Alert } from 'react-bootstrap';

const Login = (props) => {
  const [errors, setErrors] = useState('');
  const auth = useContext(AuthContext);

  const loginUser = (user) => {

    try {
      fetch(`${BASE_URL}/session/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
      })
        .then(r => r.json())
        .then(({ error, token }) => {
          if (error) {
            const errorMessage = typeof error === 'object' ? Object.values(error).join('\n') : error;
            throw errorMessage;
          }
          const user = decode(token);
          localStorage.setItem('token', token);
          auth.setAuthState({ user, isLoggedIn: true });
          props.history.push('./cars');
        })
        .catch(error => setErrors(error.toString()));

    } catch (error) {
      setErrors(error.toString())
    }
  }

  const { register, handleSubmit, formState: { errors: formErrors } } = useForm();
  return (
    <>
      <Helmet>
        <title>Login | Home</title>
      </Helmet>
      <h1>Sign-In</h1>
      <div className="login">        {errors &&

        <Alert variant={'danger'}>
          {errors}
        </Alert>
      }
        <form onSubmit={handleSubmit(loginUser)}>

          <input
            type="text"
            {...register('email', {
              required: 'Email is required!', pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                message: "Invalid email address"
              }
            })}
            placeholder="E-mail..."
          />
          {formErrors.email && <div className="error">{formErrors.email.message}</div>}
          <div>
            <input
              type="password"
              {...register('password', { required: 'password is required!' })}
              placeholder="Password..."
              className={formErrors.password && 'error'}
            />
            {formErrors.password && <div className="error">{formErrors.password.message}</div>}
          </div>
          <div style={{ marginLeft: '186px' }}>
            <button style={{ background: '#efefef' }} type="submit">Sign-In</button>
          </div>
          <div>
            <hr />
            <p><Link to="/register"><strong>Register!</strong></Link></p>
            <p><Link to="/reset"><strong>Forgotten password?</strong></Link></p>
          </div>
        </form>
      </div>
    </>
  );
};

export default Login;
