import { useForm } from 'react-hook-form';
import { Alert } from 'react-bootstrap';
import { BASE_URL } from '../../common/variables';
import { useEffect, useState } from 'react';

const ManageCustomer = ({ action, userData, handleUser, handleClose }) => {
  const [message, setMessage] = useState([]);

  const handleManage = (data) => {
    action === 'edit' ? updateUser(data) : createUser(data);

  }

  const createUser = (user) => {
    fetch(`${BASE_URL}/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(user),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          const errorMessage = typeof result.error === 'object' ? Object.values(result.error) : result.error;
          throw errorMessage;
        }
        setMessage(['success', ['Customer was successfully created!']]);
        setTimeout(() => {
          setMessage([]);
          handleUser(action, result)
        }, 2000);
      })
      .catch((error) => {
        setMessage(['danger', error]);
      });
  }

  const updateUser = (user) => {

    fetch(`${BASE_URL}/users/${userData.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(user),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          const errorMessage = typeof result.error === 'object' ? Object.values(result.error) : result.error;
          throw errorMessage;
        }
        setMessage(['success', ['Customer info was successfully updated!']]);
        setTimeout(() => {
          setMessage([]);
          handleUser(action, user)
        }, 2000);
      })
      .catch((error) => {
        setMessage(['danger', error]);
      });
  }

  const { setValue, register, handleSubmit, formState: { errors: formErrors } } = useForm({ mode: 'onSubmit' });
  useEffect(() => {
    setValue("firstName", userData.firstName);
    setValue("lastName", userData.lastName);
    setValue("email", userData.email);
    setValue("phone", userData.phone);
    setValue("address", userData.address);
    setValue("id", userData.id);

  }, [userData]);
  return (
    <>
      {action === 'edit' ? <h1>Edit customer: <span style={{ color: 'black' }}>{userData.fullName}</span></h1> :
        <h1>Create customer</h1>}
      <div className="contentBox">
        <div className="register">
          {message[1] &&
            <Alert variant={message[0]}>
              {message[1].map(e => <div key={e}>{e}</div>)}
            </Alert>
          }
          <form onSubmit={handleSubmit(handleManage)}>
            {/* {userData.id} */}
            <input type="hidden" {...register('id')} />
            <label htmlFor="firstName">First name *: </label>
            <input
              {...register('firstName', { required: 'First name is required!' })}
              type="text"
              placeholder="First name..."
              className={formErrors.firstName && 'error'}
            />
            {formErrors.firstName && <div className="error">{formErrors.firstName.message}</div>}
            <br />
            <br />

            <label htmlFor="lastName">Last name *: </label>
            <input
              {...register('lastName', { required: 'Last name year is required!' })}
              type="text"
              placeholder="Last name..."
              className={formErrors.lastName && 'error'}
            />
            {formErrors.lastName && <div className="error">{formErrors.lastName.message}</div>}
            <br />
            <br />

            <label htmlFor="email">Email*:&nbsp;</label>
            <input
              type="text"
              {...register('email', {
                required: 'Email is required!', pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                  message: "Invalid email address"
                }
              })}
              placeholder="Email..."
            />
            {formErrors.email && <div className="error">{formErrors.email.message}</div>}

            <br />
            <br />

            <label htmlFor="address">Address *: </label>
            <input
              {...register('address', { required: 'Address is required!' })}
              type="text"
              placeholder="Address..."
              className={formErrors.address && 'error'}
            />
            {formErrors.address && <div className="error">{formErrors.address.message}</div>}
            <br />
            <br />

            <label htmlFor="phone">Phone*:&nbsp;</label>
            <input
              type="text"
              {...register('phone', {
                required: 'Phone is required!', pattern: {
                  value: /^[0-9]{10}$/i,
                  message: "Invalid phone!"
                }
              })}
              placeholder="Phone..."
            />
            {formErrors.phone && <div className="error">{formErrors.phone.message}</div>}
            <br />
            <br />

            <button type="button" onClick={handleClose} style={{ width: '40%' }}>Cancel</button>&nbsp;<button type="submit" style={{ width: '40%' }}>Save</button>
          </form>
        </div>
      </div>

    </>
  )
}

export default ManageCustomer;