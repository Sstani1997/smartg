const CustomerCars = ({ carsList }) => {

  return (
    <>
      <table className="list-table">
        <tbody>
          {carsList.map((car) => {
            return (
              <tr key={car.carId}>
                <td >{car.brand} {car.model}</td>
                <td>{car.year}</td>
                <td>{car.transmission}</td>
                <td>{car.vin}</td>
                <td><span className="car-plate">{car.plate}</span></td>
                {/* <td><button onClick={() => history.push(`/cars/${car.id}`)}>Edit</button></td> */}
              </tr>
            )
          })
          }
        </tbody>
      </table>
    </>
  )
}
export default CustomerCars;