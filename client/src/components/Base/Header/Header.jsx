import Navigation from "../Navigation/Navigation";

const Header = () => {

  return (
    <header className="site-header" id="top-header">
      <div className="nav-wrapper">
        <Navigation />
      </div>
    </header>
  );
};

export default Header;
