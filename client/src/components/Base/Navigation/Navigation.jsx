import { useContext } from 'react';
import { Link, NavLink, withRouter } from 'react-router-dom';
import AuthContext from '../../../providers/authContext';
import './Navigation.scss';

const Navigation = (props) => {

  const logout = () => {
    localStorage.removeItem('token');
    auth.setAuthState({
      user: null,
      isLoggedIn: false,
    });
    props.history.push('./home');
  };

  const auth = useContext(AuthContext);
  return (
    <nav className="site-nav">
      {/* <img src="/images/logo-white.png" alt="Logo" /> */}
      <ul id="main-menu">
        <li className="home"><NavLink exact to={`/`} className="top-menu-link">Home</NavLink></li>
        {auth.isLoggedIn ?
          <>
            {/* <li className="account"><NavLink to={`/${lang}/account`} className="top-menu-link">{tr.account[lang]}</NavLink></li> */}
            {/* <li className="register"><NavLink to={`/register`} className="top-menu-link">Register</NavLink></li> */}
            <li className="cars"><NavLink to={`/cars`} className="top-menu-link">Cars</NavLink></li>
            <li className="customers"><NavLink to={`/customers`} className="top-menu-link">Customers</NavLink></li>
            <li className="orders"><NavLink to={`/orders`} className="top-menu-link">Orders</NavLink></li>
            <li className="orders"><NavLink to={`/services`} className="top-menu-link">Services</NavLink></li>
            <li className="logout"><Link to={`/`} onClick={logout} className="top-menu-link">Logout</Link></li>
          </>
          :
          <>
            <li className="login"><NavLink to={`/login`} className="top-menu-link">Login</NavLink></li>
          </>
        }
      </ul>
    </nav>
  );
};

export default withRouter(Navigation);
