import { Link } from "react-router-dom";
import translations from "../../../translations/navigation";

const PreviewHome = ({ book, lang }) => {

  return (
    <div className="book-preview">
      <div key="title" className="title">
        {book.name}
      </div>
      <div key="author" className="author">
      {translations.by[lang]} {book.author}
      </div>
      <div key="genre" className="genre">
        {book.genre}
      </div>
      <div>
          <img src={`${book.poster}`} alt={`${book.name}`} width={200}/> 
      </div>
      <Link to={`./books/${book.id}`}>View details</Link>
    </div>
  );
};

export default PreviewHome;
