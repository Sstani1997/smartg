// import { useEffect, useState } from "react";
// import { BASE_URL } from "../../../common/variables";
import React from 'react';
import './home.css';




const Home = (props) => {

  return (
    <div className="home-page">
      <img className="home-image"src="https://www.i70autoservice.com/Files/images/slideshow/slide1.jpg" alt="Service" />
    <div className="home-services row">
      <div className="col-lg-4 col-md-4">
        <div className="shadow-box-fill jobs-list">
          <ul>
            <li><a href="/services" className="search__popular__service d-flex no-padding">
              <div className="search__popular__service__icon"><i className="icon car-not-starting"></i></div>Car is not starting
                    Inspection
                  </a></li>
            <li><a className="search__popular__service d-flex no-padding" href="/services">
              <div className="search__popular__service__icon"><i className="icon oil-change"></i></div>Change Oil and Filter
                  </a></li>
            <li><a className="search__popular__service d-flex no-padding" href="/services">
              <div className="search__popular__service__icon"><i className="icon brake-pads"></i></div>Brake Pads Replacement
                  </a></li>
            <li><a className="search__popular__service d-flex no-padding" href="/services">
              <div className="search__popular__service__icon"><i className="icon rnr"></i></div>Timing Belt Replacement
                  </a></li>
            <li><a className="search__popular__service d-flex no-padding" href="/services">
              <div className="search__popular__service__icon"><i className="icon pre-purchase"></i></div>Pre-purchase Car
                    Inspection
                  </a></li>
            <li><a className="search__popular__service d-flex no-padding" href="/services">
              <div className="search__popular__service__icon"><i className="icon battery"></i></div>Battery Replacement
                  </a></li>
          </ul>
        </div>
      </div>
      <div className="col-lg-4 col-md-4">
        <div className="shadow-box-fill jobs-list">
          <ul>
            <li><a className="search__popular__service d-flex no-padding" href="/services">
              <div className="search__popular__service__icon"><i className="icon starter"></i></div>Starter Replacement
                  </a></li>
            <li><a className="search__popular__service d-flex no-padding" href="/services">
              <div className="search__popular__service__icon"><i className="icon alternator"></i></div>Alternator Replacement
                  </a></li>
            <li><a className="search__popular__service d-flex no-padding" href="/services">
              <div className="search__popular__service__icon"><i className="icon rnr"></i></div>Serpentine/Drive Belt
                    Replacement
                  </a></li>
            <li><a className="search__popular__service d-flex no-padding" href="/services">
              <div className="search__popular__service__icon"><i className="icon spark-plugs"></i></div>Spark Plugs Replacement
                  </a></li>
            <li><a className="search__popular__service d-flex no-padding" href="/services">
              <div className="search__popular__service__icon"><i className="icon water-pump"></i></div>Water Pump Replacement
                  </a></li>
            <li><a className="search__popular__service d-flex no-padding" href="/services">
              <div className="search__popular__service__icon"><i className="icon fuel-pump"></i></div>Fuel Pump Replacement
                  </a></li>
          </ul>
        </div>
      </div>
      <div className="col-lg-4 col-md-4">
        <div className="shadow-box-fill jobs-list ">
          <ul>
            <li><a className="search__popular__service d-flex no-padding" href="/services">
              <div className="search__popular__service__icon"><i className="icon radiator"></i></div>Radiator Replacement
                  </a></li>
            <li><a className="search__popular__service d-flex no-padding" href="/services">
              <div className="search__popular__service__icon"><i className="icon rnr"></i></div>Valve Cover Gasket Replacement
                  </a></li>
            <li><a className="search__popular__service d-flex no-padding" href="/services">
              <div className="search__popular__service__icon"><i className="icon rnr"></i></div>Oxygen Sensor Replacement
                  </a></li>
            <li><a className="search__popular__service d-flex no-padding" href="/services">
              <div className="search__popular__service__icon"><i className="icon rnr"></i></div>Thermostat Replacement
                  </a></li>
            <li><a className="search__popular__service d-flex no-padding" href="/services">
              <div className="search__popular__service__icon"><i className="icon rnr"></i></div>Wheel Bearings Replacement
                  </a></li>
            <li><a className="search__popular__service d-flex no-padding" href="/services">
              <div className="search__popular__service__icon"><i className="icon rnr"></i></div>Axle/CV Shaft Assembly
                    Replacement
                  </a></li>
          </ul>
        </div>
      </div>
    </div>
    </div>
  );
};


export default Home;