import { useEffect, useState } from 'react';
import { BASE_URL } from '../../../common/variables.js';


const PasswordReset = ({ location }) => {
    const [response, setResponse] = useState();

    const resetPassword = (requestId) => {
        fetch(`${BASE_URL}/session/forgotten?ID=${requestId}`)
            .then(r => r.json())
            .then(setResponse)
            .catch(console.log);
    }

    useEffect(() => {
        if (location.search !== "" && location.search.includes("ID")) {
            const requestId = location.search.replace("?ID=", "");
            resetPassword(requestId);
        }
    }, []);

    if (response && response.error) {
        return (
            <>
                <div className="reset-password-container">
                    <i class="fas fa-times"></i>
                    <p >{response && response.error}</p>
                </div>
            </>
        )
    } else if (response) {
        return (
            <>
                <div className="reset-password-container">
                    <i class="fa fa-check"></i>
                    <p >{response && response.message}</p>
                </div>
            </>
        )
    } else {
        return (
            <>
                <div className="reset-password-container">
                    <h3>Loading...</h3>
                </div>
            </>
        )
    }


};


export default PasswordReset;