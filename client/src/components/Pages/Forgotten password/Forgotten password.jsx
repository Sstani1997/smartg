import { useState } from 'react';
import { BASE_URL } from '../../../common/variables.js';
import { useForm } from 'react-hook-form';
import { Helmet } from 'react-helmet-async';
import { Alert } from 'react-bootstrap';

 
const ForgotPassword = ({ history }) => {
    const [errors, setErrors] = useState('');
    // const [registered, setRegistered] = useState({});
    const passwordRequest = (user) => {
      fetch(`${BASE_URL}/session/passwordRequest`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(user),
      })
        .then(r => r.json())
        .then(({ error }) => {
          if (error) {
            const errorMessage = typeof error === 'object' ? Object.values(error).join('\n') : error;
            setErrors(errorMessage.toString());
          }
          // setRegistered(user)
          history.push('./login');
        })
        .catch(error => setErrors(error.toString()));
    };
  
    const { register, handleSubmit, formState: { errors: formErrors, isValid } } = useForm({ mode: "onChange" });
    return (
      <>
  
        <Helmet>
          <title>Reset | Home</title>
        </Helmet>
        <h1>Forgot your password?</h1>
        <div className="register">
          {errors &&
            <Alert variant={'danger'}>
              {errors}
            </Alert>
          }
          <form onSubmit={handleSubmit(passwordRequest)}>
          <h4>
              Enter the email address that you registered with us 
              and we will send you further instructions.
          </h4>
            <label htmlFor="email">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E-mail*:&nbsp;</label>
            <input
              type="text"
              {...register('email', {
                required: 'Email is required!', pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                  message: "Invalid email address"
                }
              })}
              placeholder="E-mail..."
            />
            {formErrors.email && <div className="error">{formErrors.email.message}</div>}
            <button disabled={!isValid} type="submit">Reset password</button>
          </form>
        </div>
      </>
    );
  };
 
export default ForgotPassword;