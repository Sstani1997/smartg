import { Redirect, Route } from 'react-router';

const GuardedRoute = ({ component: Component, isLoggedIn, ...rest },props) => {
  return (
    <Route
      {...rest}
      render={(props) =>
        isLoggedIn ? <Component {...props} /> : <Redirect to={`/login`} />
      }
    />
  );
};

export default GuardedRoute;
