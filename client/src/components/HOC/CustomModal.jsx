import { Modal } from 'react-bootstrap'

const CustomModal = ({ children, component, show, handleClose}) => {


  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Body>
        {children}
      </Modal.Body>
    </Modal>
  );


}
export default CustomModal;