export const BASE_URL = 'http://localhost:5555/api';
export const AUTH_HEADER = {
    'Authorization': `Bearer ${localStorage.getItem('token')}`
}