# ***Smart Garage***
<br>

## Table of contents
   - [1. Description](#1-description)
   - [2. Project information](#2-project-information)
   - [3. Setup](#3-setup)
   - [4. Project structure](#4-project-structure)
   - [5. Working with Postman and postman collections](#5-working-with-postman-and-postman-collections)
   - [6. Documentation - Backend ](#6-documentation---backend)
   - [7. Documentation - Frontend ](#7-documentation---frontend)


### Creators:
- Stanislav Botev 😁 
- Svetoslav Yordanov 😁

### **1. Description**
<br>
SmartG is a platform where you can browse between services, check all the cars and customers that have been in the garage, make new registration to a customer and send him info via email.

<br>

### **2. Project information**

<br>

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages: **Express**, **ESLint**, **React**
- Libraries: **Fontawesome**

<br>

### **3. Setup**
<br>

You will be working in the `src` folder. This will be designated as the **root** folder, where `package.json` should be placed.

You need to install all the packages in the root folder: `npm i`.

The project can be run in two ways:

- `npm start` - will run the current version of the code and will **not** reflect any changes done to the code until this command is executed again
- `npm run start:dev` - will run the code in *development* mode, meaning every change done to the code will trigger the program to restart and reflect the changes made
- `npm run eslint` - will run the **eslint** file 
- `npm start` - root client starts the frontend part in folder client **root**, where `package.json` should be placed

**Create Environment Variable** - .env file in root **server** folder
```
PORT=5555
HOST=triton.rdb.superhosting.bg
USER=fwraptea_bibuser
PASSWORD=kakavida
DATABASE=fwraptea_smartg
DBPORT=3306

SECRET_KEY=SECRET_KEY

```
**Setup DB**
1. Make sure MariaDB database service is running
2. Start your  SQL Client and create new database scheme 
3. Connect to the database .
4. Use the database for the project.

<br>

### 4. Project structure

Project structure consists of two main parts - back-end (folder `server`) and front-end (folder `client`)

**Back-end - folder (server)**

- `src/index.js` - the entry point of the backend project
- `src/auth` - contain settings and logic for authentication 
- `src/common` - contains helper files `error`, `error-strings`, `helpers`, `send-mails`, `variables` 
- `src/controllers` - contains all controller logic `auth`, `orders`, `service`, `users`, `cars`, ` session`
- `src/data` - contains the connection settings and functions for CRUD over users, services, orders,vehicles in the database
- `src/middleware` -  contains all custom middleware
- `src/services` - contains all service logic
- `src/validators` - contains objects (called schemas) for validating body/query objects

**Front-end - folder (client)**

- `src/App.js` - the entry point of the frontend project
- `src/common` - contains `variables`
- `src/components` - contains components of resources
- `src/providers` - contains `authContext`, `data-list`

<br>

### 5. Working with Postman and postman collections
1. Download and Install Postman
2. Open the **Postman** tool
3. Click the **Import** button and choose files in the **postman** folder

<br>


### 6. Documentation - Backend

**The following CRUD operations and endpoints are implemented**

<br>

**Public endpoints**

 The public part of the smart garage is accessible without authentication.

  User properties
   - `email` - A valid email address in the range `[3-100]` by which the user is registered in the garage system
   - `password` - string with length in the range `[8-20]`, which user has received after registration in the garage system via email
  
 Endpoints
   - `POST: /login` - login user
     - body: `{ email: String, password: String}`, validation criteria provided above; 
   - `POST: /forgot` - forgot password, which check if user exist in the system and created a token for resetting forgotten password 
     - body: `{ email: String }`, validation criteria provided above; 
   - `POST:/reset` - reset a forgotten password of a user, who is already existing in the system
     - body: `{ password: String }`, validation criteria provided above; 

<br>

**Private endpoints**


  User/Customer properties


   - `firstName` - string with length in the range `[2-20]` 
   - `lastName` - string with length in the range `[2-20]`
   - `email` - a valid email format, validated via regex
   - `phone` - Expected a valid phone format 
   - `address` - Expected string 
  
  <br>

  Vehicle properties

   - `plate` - Expected string a valid Bulgarian license plate 
   - `year` - Expected year format `XXXX - 1900 - 2099 `
   - `vin` - Expected a valid exactly 17 long string BG vehicle identification number
   - `transmissions` - Expected string `manual`, `automatic`
   - `engine` - Expected string 
   - `model` - Expected string in the range `[1-20]`
   - `brand` - Expected a valid brand from our garage -> see our brand list 
  
  <br>

  Service properties

   - `name` - Expected string in the range `[3-45]`
   - `price` - Expected number

  <br>

  Order properties

   - `date_in` - Expected a Valid Date
   - `date_out` - Expected a Valid Date
   - `status` - Expected string: `new`, `in progress` or `done`

  <br>  

 Endpoints User/Customer
   - `DELETE: /logout` - logout the current user and deleting token from localStorage
   - `GET: /user/order ` - get an order by a user
   - `GET: /services` - view all services of the garage system
   - `PUT: /users/:id` - updates the current user profile information 

<br>

 Endpoints User/Employee

   - `GET: /users` - retrieve all users
   - `POST: /users` - creates a new user and send an email login email and password
     - body: `{ firstName: String, lastName:String, email:String, phone: String, address: String, }`, validation criteria provided above and if no avatar the default value is used; 
   - `DELETE: /users/:id` - deletes existing user by id
   - `GET: /vehicles` - retrieve all vehicles
   - `PUT: /vehicles/:id` - updates an existing vehicle
     - body: `{ plate: String, year:String, transmissions: String, engine: String, model: String, brand: String  }`, validation criteria provided above;
   - `POST: /vehicles` - creates a new vehicle
     - body: `{ plate: String, year:String, vin:String, transmissions: String, engine: String, model: String, brand: String, customer_id: Number  }`, validation criteria provided above;
   - `DELETE: /vehicles/:id` - deletes existing vehicle by id
   - `PUT: /services/:id` - updates price by service id
     - body: `{ price: Number  }`, validation criteria provided above; 
   - `DELETE: /services/:id` - deletes existing service by id
<br>

### 7. Documentation - Frontend

**Public routes**

   - `Route - /` - Redirecting to `/home` route
   - `Route - /home` - directs to `Home` component 
   - `Route - /login` - directs to `Login` component
   - `Route - /forgot` - directs to `ForgotPassword` component
   - `Route - /reset` - directs ti `ResetPassword` component 
   - `Route - /*` - directs to component `NotFound` in case no resource is found

  
**Private routes- User/Customer**

   - `Route - /services` - directs to `ServiceUser` component, only if the user is logged in
   - `Route - /profile` - directs to `Profile` component and render detailed information about current customer profile


<br>
<br>

**Homepage View**

![Home](./client/public/images/Home.png)


<br>
<br>


**Sign-in View**

![Sign-in](./client/public/images/Sign-in.png)


<br>
<br>


**Register View**

![Register](./client/public/images/Register.png)


<br>
<br>


**Reset password View**

![Reset](./client/public/images/Reset.png)


<br>
<br>


**Services View**

![Services](./client/public/images/Services.png)


<br>
<br>


**Orders View**

![Orders](./client/public/images/Orders.png)


<br>
<br>


**Dashboard/Cars View**

![Cars](./client/public/images/Cars.png)


<br>
<br>


**Dashboard/Customers View**

![Customers](./client/public/images/Customers.png)


<br>
<br>
